<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<sql:query var="modelos" dataSource="jdbc/classicmodels">
    select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Catálogo de Productos</title>
	<style>
  h1 {  font-family: 'Helvetica Neue', sans-serif; font-size: 100px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center;background-image: linear-gradient(to right,purple , yellow); color:white; }
  p { color: #685206; font-family: 'Helvetica Neue', sans-serif; font-size: 14px; line-height: 24px; margin: 0 0 24px; text-align: justify; text-justify: inter-word;  }
  h3 {  font-family: 'Helvetica Neue', sans-serif; font-size: 25px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center;background-image: linear-gradient(to right,purple , yellow); color:white; }
  
  
.Buttons {
	box-shadow:inset 0px 1px 0px 0px #efdcfb;
	background:linear-gradient(to bottom, purple 5%, yellow 100%);
	background-color:#dfbdfa;
	border-radius:6px;
	border:1px solid #c584f3;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #9752cc;
	
}
.Buttons:hover {
	background:linear-gradient(to bottom, #bc80ea 5%, #dfbdfa 100%);
	background-color:#bc80ea;
}
.Buttons:active {
	position:relative;
	top:1px;
}

     
  
</style>
<body>
	<h1>Catálogo de Modelos a Escala</h1>
	<hr/>
	<c:choose>
		<c:when test="${not empty sessionScope.customer}">
			<p>${sessionScope.customer}</p>
			<p><a href="cart.jsp" class="Buttons">Carrito</a></p>
			<p><a href="logout.jsp" class="Buttons">Cerrar Sesión</a></p>
		</c:when>
		<c:otherwise>
			<p><a href="login.jsp" class="Buttons">Iniciar Sesión</a></p>
			<p><a href="registro.jsp" class="Buttons">Regístrate</a></p>
			
		</c:otherwise>
	</c:choose>
	<c:forEach var="modelo" items="${modelos.rows}">
		<h3>${modelo.productName}</h3>
		<p>Escala ${modelo.productScale}</p>
		<p>${modelo.productDescription}"</p>
		<p>
			Precio: ${modelo.MSRP}
			<c:if test="${not empty sessionScope.customer}">
				<a href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">
					<img style="width: 1em; height: 1em;" src="img/cart-plus.svg" />
				</a>
				
			</c:if>
		</p>
		<hr/>
	</c:forEach>
</body>
</html>