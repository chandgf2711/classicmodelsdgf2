<%@ page language ="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%
if (session.getAttribute("user") != null)
	response.sendRedirect("catalogo");
else {
	String firstName = request.getParameter("nombre");
	String lastName = request.getParameter("apellidos");
	String email = request.getParameter("email");
	if (firstName == null || lastName == null || email == null)
		response.sendRedirect("catalogo.jsp");
	else {
%><!DOCTYPEhtml>
<html>
<head>
<metacharset="ISO-8859-1">
<title>Registro</title>
<style>
 
   h1 {  font-family: 'Helvetica Neue', sans-serif; font-size: 100px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center;background-image: linear-gradient(to right,purple , yellow); color:white; }
  p { color: #685206; font-family: 'Helvetica Neue', sans-serif; font-size: 14px; line-height: 24px; margin: 0 0 24px; text-align: justify; text-justify: inter-word;  }
  h3 {  font-family: 'Helvetica Neue', sans-serif; font-size: 25px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center;background-image: linear-gradient(to right,purple , yellow); color:white; }
  
  
.Buttons {
	box-shadow:inset 0px 1px 0px 0px #efdcfb;
	background:linear-gradient(to bottom, purple 5%, yellow 100%);
	background-color:#dfbdfa;
	border-radius:6px;
	border:1px solid #c584f3;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #9752cc;
	
}
.Buttons:hover {
	background:linear-gradient(to bottom, #bc80ea 5%, #dfbdfa 100%);
	background-color:#bc80ea;
}
.Buttons:active {
	position:relative;
	top:1px;
}

     
  
</style>
</head>
<%
System.out.println("hola");
%><body>
	<h1>
		Bienvenido
		<%=firstName%></h1>
	<p>
		Sólo queda un paso más para completar tu registro y tener acceso a
		todos nuestrosServicios, sigue las instrucciones que te hemos enviado
		a
		<%=email%></p>
	<p>Pulsa el botón de reenviar para que te volvamos a enviar el
		mensaje de confirmación:</p>
	<form action ="reenvio"method="post"> 
	<input type="hidden"name="email" value="<%=email%>" />
	<input type="hidden"name="nombre" value="<%=firstName%>" />
	<input type="hidden"name="apellidos" value="<%=lastName%>" />
	<p>
		<input type ="submit" class="Buttons" value="Reenviar" />
	</p>
	
	</p>
	</form>
	<p>
		<a href="login.jsp" class="Buttons">Iniciar sesión</a>
	</p>
	<p>
		<a href="catalogo.jsp" class="Buttons">Página de inicio</a>
	</p>
</body>
</html>
<%
}
}
%>