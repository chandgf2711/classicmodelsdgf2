<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Carrito</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
	<style>
  h1 {  font-family: 'Helvetica Neue', sans-serif; font-size: 100px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center;background-image: linear-gradient(to right,purple , yellow); color:white; }
  p { color: #685206; font-family: 'Helvetica Neue', sans-serif; font-size: 14px; line-height: 24px; margin: 0 0 24px; text-align: justify; text-justify: inter-word;  }
  h3 {  font-family: 'Helvetica Neue', sans-serif; font-size: 25px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center;background-image: linear-gradient(to right,purple , yellow); color:white; }
  
  
.Buttons {
	box-shadow:inset 0px 1px 0px 0px #efdcfb;
	background:linear-gradient(to bottom, purple 5%, yellow 100%);
	background-color:#dfbdfa;
	border-radius:6px;
	border:1px solid #c584f3;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #9752cc;
	
}
.Buttons:hover {
	background:linear-gradient(to bottom, #bc80ea 5%, #dfbdfa 100%);
	background-color:#bc80ea;
}
.Buttons:active {
	position:relative;
	top:1px;
}

}

     
  
</style>
</head>
<body>
	<h1>Carrito de la compra</h1>
	<p><a href="catalogo.jsp" class="Buttons">Catálogo</a></p>
	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
			<table class="table">
  <thead class="table-danger">
    <tr>
      <th scope="col">Producto</th>
      <th scope="col">Unidades</th>
      <th scope="col">Precio</th>
      <th scope="col">Importe</th>
    </tr>
  </thead>
  <tbody>
				<c:forEach var="linea" items="${sessionScope.cart}">
					<tr class="table-warning">
					
						<td>${linea.value.productName}</td>
						<td>${linea.value.amount}</td>
						<td>${linea.value.price}</td>
						<td>${linea.value.amount * linea.value.price}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</c:otherwise>		
	</c:choose>
</body>
</html>
