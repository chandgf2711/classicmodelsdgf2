<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Inicio de Sesión</title>
	<style>
 
  p label { color: #685206; font-family: 'Helvetica Neue', sans-serif; font-size: 14px; line-height: 24px; margin: 0 0 24px; text-align: justify; text-justify: inter-word;  }
  
 .inputs {
  max-width: 250px;
  padding: 1rem;
  position: relative;
  background: linear-gradient(to right, purple, yellow);
  padding: 3px;
  color:white;
}


  
  
.Buttons {
	box-shadow:inset 0px 1px 0px 0px #efdcfb;
	background:linear-gradient(to bottom, purple 5%, yellow 100%);
	background-color:#dfbdfa;
	border-radius:6px;
	border:1px solid #c584f3;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #9752cc;
	
}
.Buttons:hover {
	background:linear-gradient(to bottom, #bc80ea 5%, #dfbdfa 100%);
	background-color:#bc80ea;
}
.Buttons:active {
	position:relative;
	top:1px;
}

     
  
</style>
	</head>
	<body>
		<form action="auth" method="post">
			<p><label for="email">eMail</label></p>
			<p><input type="email" class="inputs"  name="email" required="required" /></p>
			<p><label for="password">Contraseña</label></p>
			<p><input type="password" class="inputs" name="password" required="required"/></p>
			<p><input type="submit"  class="Buttons"value="Login" /></p>
		</form>	
		<c:choose>
			<c:when test="${param.status == 1}">
				<p>Las credenciales del usuario no son válidas</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del sistema, contacte con el administrador</p>
			</c:when>
			<c:when test="${param.status == 3}">
				<%
				String firstName =(String) session.getAttribute("nombre") ;
				String lastName = (String) session.getAttribute("apellidos");
				String email =(String) session.getAttribute("email");
				response.sendRedirect("postregistro.jsp?email=" + email+"&nombre="+firstName+"&apellidos="+lastName); %>
			</c:when>
		</c:choose>
	</body>
</html>
