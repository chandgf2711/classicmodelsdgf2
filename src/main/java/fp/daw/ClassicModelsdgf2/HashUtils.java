package fp.daw.ClassicModelsdgf2;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class HashUtils {

	public static byte[] getHash(String password, int lenght, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 0xffff, lenght);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		SecretKey key = factory.generateSecret(spec);
		return key.getEncoded();
	}

	private static SecureRandom secureRandom = new SecureRandom();

	public static byte[] getRandomSalt(int length) {
		byte[] salt = new byte[length];
		secureRandom.nextBytes(salt);
		return salt;
	}

	public static String getBase64Hash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] salt = getRandomSalt(66);
		byte[] hash = getHash(password, 512, salt);
		String hashBase64 = Base64.getEncoder().encodeToString(hash);
		String saltBase64 = Base64.getEncoder().encodeToString(salt);
		return hashBase64.substring(0, 21) + saltBase64 + hashBase64.substring(21);
	}

	public static boolean validateBase64Hash(String password, String savedHash)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] salt = Base64.getDecoder().decode(savedHash.substring(21, 109));
		byte[] hash1 = Base64.getDecoder().decode(savedHash.substring(0, 21) + savedHash.substring(109));
		byte[] hash2 = getHash(password, 512, salt);
		return Arrays.compare(hash1, hash2) == 0;
	}

	public static String getBase64Digest(String message) throws NoSuchAlgorithmException {
		byte[] hash = MessageDigest.getInstance("SHA-256").digest(message.getBytes());
		return Base64.getUrlEncoder().encodeToString(Arrays.copyOf(hash, 33));
		/* retorna una cadena de longitud 44 */ }

}
