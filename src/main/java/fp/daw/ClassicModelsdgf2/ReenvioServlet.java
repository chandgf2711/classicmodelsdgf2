package fp.daw.ClassicModelsdgf2;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class ReenvioServlet
 */
@WebServlet("/reenvio")
public class ReenvioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String nombre = request.getParameter("nombre");
		String apellidos = request.getParameter("apellidos");
		
		Connection con = null;
		ResultSet rs =null;
		String confirmationCode=null;
		PreparedStatement stm = null;
		if (nombre == null || apellidos == null || email == null )
			response.sendRedirect("catalogo.jsp");
		else {try {
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			stm = con.prepareStatement("select confirmationCode from signups where customerEmail=? ");
			stm.setString(1, email);
			rs=stm.executeQuery();
			rs.next();
			confirmationCode =rs.getString(1);
			MailService.sendConfirmationMessage(email, confirmationCode);
			response.sendRedirect("postregistro.jsp?email=" + email+"&nombre="+nombre+"&apellidos="+apellidos);
		} catch (SQLException e) {
			
		} catch (  NamingException | MessagingException e) {
			
		} finally {
			if (stm != null)
				try {
					stm.close();
				} catch (SQLException e) {
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
				}
		}
		
		}
		
		
	}
		
		
		
		
	

		

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
